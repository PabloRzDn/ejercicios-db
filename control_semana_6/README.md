# Control 6 
## Bases de datos (PostgreSQL y PGadmin)
---

1. Cree una base de datos de nombre “Concesionario”, luego elabore las tablas utilizando las
sentencias SQL correspondientes del modelo relacional que se muestra a continuación.

```sql
create database concesionarios

------------------------------

CREATE DATABASE

Query returned successfully in 912 msec.

```
```sql

create table clientes (
	id bigserial primary key,
	nombre varchar(40) not null,
	rut varchar(10) not null,
	direccion varchar(40) not null,
	correo varchar(35) not null,
	telefono varchar(12) not null
);

create table autos(
	id bigserial primary key,
	marca varchar(20) not null,
	modelo varchar(15) not null,
	color varchar(15) not null,
	anio integer not null,
	costo_diario float not null
);

create table alquileres(
	id bigserial primary key,
	id_auto int not null,
	id_cliente int not null,
	fecha date not null,
	descripcion varchar(15) not null,
	cantidad_dias integer not null,
	costo_diario float not null,
	foreign key (id_auto) references autos(id),
	foreign key (id_cliente) references clientes(id)
);


---------------------------------
CREATE TABLE

Query returned successfully in 189 msec.

```
2. Realice las sentencias SQL correspondientes para llenar las tablas Autos, Clientes y Alquileres
con los datos antes indicados.

```sql

insert into autos (marca,modelo,color,anio,costo_diario) values
	('Ford','fiesta','blanco',2015,100000),
	('Chevrolet','sail','azul',2017,120000),
	('Toyota','tacoma','rojo',2014,180000),
	('Ford','ecosport','azul',2018,180000),
	('Toyota','4runner','negro',2019,250000),
	('Ford','explorer','negro',2015,220000),
	('Nissan','versa','azul',2016,130000),
	('Chevrolet','Orlando','gris',2014,180000),
	('Mercedes Benz','a200','blanco',2018,290000),
	('Ford','ecosport','azul',2018,180000);

insert into clientes (nombre,rut,direccion,correo,telefono) values
	('Carmen Jara','18345234-2','San Antonio 786','cjara@gmail.com','912342233'),
	('Pamela Reyes','16765123-k','Las Nieves 1485','preyes@gmail.com','988775532'),
	('Daniela Cataldo','23987454-4','Colchagua 2244','dcataldo@gmail.com','990442354'),
	('Victor Perez','25455778-1','San diego 1310','vperez@gmail.com','943223123'),
	('Manuel Rivas','20229551-3','Huerfanos 1020','mrivas@gmail.com','925768900');
	
insert into alquileres (id_auto, id_cliente, fecha, descripcion, cantidad_dias,costo_diario) values
	(1,1,'2019-10-02','Uso RM',2,100000),
	(1,2,'2019-10-05','Uso RM',3,100000),
	(3,2,'2019-10-09','Uso VI region',2,100000),
	(4,3,'2019-10-09','Uso VII region',1,180000),
	(2,1,'2019-10-10','Uso V region',1,120000),
	(2,2,'2019-10-12','Uso RM',3,120000),
	(5,3,'2019-10-12','Uso VII region',2,250000),
	(5,5,'2019-10-15','Uso V region',5,250000),
	(7,2,'2019-10-16','Uso VII region',7,130000),
	(8,4,'2019-10-16','Uso RM',4,180000)
	

---------------------------------

INSERT 0 10

Query returned successfully in 81 msec.

```

3. Realice la sentencia SQL que muestre de la tabla Autos los datos: Marca, Color y Año. De la
tabla Clientes los datos: Nombre Completo, RUT y Teléfono. De la tabla Alquileres los datos:
Fecha, Descripción y Cantidad Días, para aquellos alquileres de más de 3 días.

```sql

select clientes.nombre, clientes.rut,clientes.telefono, autos.marca, autos.color, autos.anio, alquileres.fecha, alquileres.descripcion,alquileres.cantidad_dias from alquileres
left join clientes
on alquileres.id_cliente=clientes.id
left join autos
on alquileres.id=autos.id where alquileres.cantidad_dias>3

```

4. Realice una sentencia SQL que muestre de la tabla Autos los datos: Marca, Color y Año. De la
tabla Clientes los datos: Nombre Completo, RUT y Teléfono. De la tabla Alquileres los datos:
Fecha, Descripción y Cantidad Días, para aquellos alquileres de menos de 3 días y que el año del
vehículo sea del 2015 al 2017.

```sql

select clientes.nombre, clientes.rut,clientes.telefono, autos.marca, autos.color, autos.anio, alquileres.fecha, alquileres.descripcion,alquileres.cantidad_dias from alquileres
left join clientes
on alquileres.id_cliente=clientes.id
left join autos
on alquileres.id=autos.id where alquileres.cantidad_dias<3 and autos.anio > 2015 and autos.anio<2017

```

5. Realice una sentencia SQL que muestre de la tabla Autos los datos: Marca, Color y Año. De la
tabla Clientes los datos: Nombre Completo, RUT y Teléfono. De la tabla Alquileres los datos:
Fecha, Descripción y Cantidad Días, para aquellos alquileres de menos de 3 días y que el alquiler
se realizará en la Región Metropolitana.


```sql
select clientes.nombre, clientes.rut,clientes.telefono, autos.marca, autos.color, autos.anio, alquileres.fecha, alquileres.descripcion,alquileres.cantidad_dias from alquileres
left join clientes
on alquileres.id_cliente=clientes.id
left join autos
on alquileres.id=autos.id where alquileres.cantidad_dias<3 and alquileres.descripcion='Uso RM'

```