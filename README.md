# Bases de datos relacionales

Pablo Ruz Donoso - 2022

---

El presente repositorio contiene diversos ejercicios resueltos en torno a bases de datos relacionales. Se utiliza **PostgreSQL** y **Pgadmin4**. Los contenidos hasta el momento son:

- Creación de bases de datos.
- Creación de tablas.
- Eliminación de tablas.
- Selección de datos de tablas (mostrar datos).
- Bases de datos relacionales.
- Claves foráneas.
- Tablas con múltiples datos (*select <...> join*)

