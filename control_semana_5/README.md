# Control 5 
## Bases de datos (PostgreSQL y PGadmin)
---


1. Cree una base de datos de nombre "Concesionario", y utilizando como guía o modelo la tabla que se muestra a continuación, responda ¿Cómo ejecutar sentencias SQL que permitan crear la base de datos antes indicada, además de la tabla descrita anteriormente?

**Respuesta**

```sql
create database Concesionarios
```

![Crear Base de Datos](./screenshots/create_database.png)

```sql
create table Autos (
    Id bigserial primary key,
    Marca varchar(20) not null,
    Modelo varchar(15) not null,
    Color varchar(15) not null,
    Anio int not null,
    Precio float not null
)
```
![crear Tabla](./screenshots/create_table.png)0v011d4.16.


2. Considerando la tabla creada en la pregunta anterior ¿Cómo generaría las sentencias SQL necesarias para llenar la tabla con los datos mostrados en la imagen que se presenta a continuación?

**Respuesta**

```sql
insert into Autos (marca, modelo, color, anio, precio) values 
('Ford','Fiesta','Blanco',2015,8500000),
('Chevrolet','Sail','Azul',2017,6500000),
('Toyota','Tacoma','Rojo',2014,22100000),
('Ford','Ecosport','Azul',2018,11200000),
('Toyota','4runner','Negro',2019,25600000),
('Ford','Explorer','Negro',2015,19900000),
('Nissan','Versa','Azul',2016,9350000),
('Chevrolet','Orlando','Gris',2014,14000000),
('Mercedes Benz','a200','Blanco',2018,25220000),
('Chevrolet','Spark','Rojo',2019,6100000)

```


![Tabla](./screenshots/table.png)

3. Considerando la tabla anterior ¿Cómo generaría una sentencia SQL para otorgar permisos de ejecución de sentencias Select a los id 1 y 6? 

```sql
create policy permisos on autos for select using (id <= (select id from autos where id in (1,6)))
```

![Permisos](./screenshots/create_policy.png)